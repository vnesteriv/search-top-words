# -*- coding: utf-8 -*-
import collections


def search_top_words():
    handle = open("input_text.txt", "r")
    input_text = handle.readlines()
    input_text = ''.join(input_text)
    handle.close()

    list_words = input_text.split(' ')

    words = []

    for row in list_words:
        if len(row) > 7:
            words.append(row.lower())

    c = collections.Counter()

    for word in words:
        c[word] += 1

    top = c.most_common(3)

    i = 1
    for row in top:
        print("{0}) the word '{1}' is found {2} times".format(i, row[0], row[1]))
        i += 1


if __name__ == "__main__":
    search_top_words()
